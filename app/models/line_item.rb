class LineItem < ApplicationRecord
  belongs_to :passagem
  belongs_to :cart

  def total_price
    passagem.preco.to_i * quantity.to_i
  end
end
