class Passagem < ApplicationRecord
  before_destroy :not_referenced_by_any_line_item
  belongs_to :Aerotransporte
  has_many :line_items

  private 
  def not_referenced_by_any_line_item
    unless line_items.empty? 
      erros.add(:base, "Line items present")
      throw :abort
    end
  end
end
