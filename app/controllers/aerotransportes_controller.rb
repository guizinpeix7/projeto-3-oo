class AerotransportesController < ApplicationController
  before_action :set_aerotransporte, only: [:show, :edit, :update, :destroy]

  # GET /aerotransportes
  # GET /aerotransportes.json
  def index
    @aerotransportes = Aerotransporte.all
  end

  # GET /aerotransportes/1
  # GET /aerotransportes/1.json
  def show
  end

  # GET /aerotransportes/new
  def new
    @aerotransporte = Aerotransporte.new
  end

  # GET /aerotransportes/1/edit
  def edit
  end

  # POST /aerotransportes
  # POST /aerotransportes.json
  def create
    @aerotransporte = Aerotransporte.new(aerotransporte_params)

    respond_to do |format|
      if @aerotransporte.save
        format.html { redirect_to @aerotransporte, notice: 'Aerotransporte was successfully created.' }
        format.json { render :show, status: :created, location: @aerotransporte }
      else
        format.html { render :new }
        format.json { render json: @aerotransporte.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /aerotransportes/1
  # PATCH/PUT /aerotransportes/1.json
  def update
    respond_to do |format|
      if @aerotransporte.update(aerotransporte_params)
        format.html { redirect_to @aerotransporte, notice: 'Aerotransporte was successfully updated.' }
        format.json { render :show, status: :ok, location: @aerotransporte }
      else
        format.html { render :edit }
        format.json { render json: @aerotransporte.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /aerotransportes/1
  # DELETE /aerotransportes/1.json
  def destroy
    @aerotransporte.destroy
    respond_to do |format|
      format.html { redirect_to aerotransportes_url, notice: 'Aerotransporte was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_aerotransporte
      @aerotransporte = Aerotransporte.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def aerotransporte_params
      params.require(:aerotransporte).permit(:nome)
    end
end
