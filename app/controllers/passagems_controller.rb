class PassagemsController < ApplicationController
  before_action :set_passagem, only: [:show, :edit, :update, :destroy]

  # GET /passagems
  # GET /passagems.json
  def index
    @passagems = Passagem.all
  end

  # GET /passagems/1
  # GET /passagems/1.json
  def show
  end

  # GET /passagems/new
  def new
    @passagem = Passagem.new
  end

  # GET /passagems/1/edit
  def edit
  end

  # POST /passagems
  # POST /passagems.json
  def create
    @passagem = Passagem.new(passagem_params)

    respond_to do |format|
      if @passagem.save
        format.html { redirect_to @passagem, notice: 'Passagem was successfully created.' }
        format.json { render :show, status: :created, location: @passagem }
      else
        format.html { render :new }
        format.json { render json: @passagem.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /passagems/1
  # PATCH/PUT /passagems/1.json
  def update
    respond_to do |format|
      if @passagem.update(passagem_params)
        format.html { redirect_to @passagem, notice: 'Passagem was successfully updated.' }
        format.json { render :show, status: :ok, location: @passagem }
      else
        format.html { render :edit }
        format.json { render json: @passagem.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /passagems/1
  # DELETE /passagems/1.json
  def destroy
    @passagem.destroy
    respond_to do |format|
      format.html { redirect_to passagems_url, notice: 'Passagem was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_passagem
      @passagem = Passagem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def passagem_params
      params.require(:passagem).permit(:origem, :destino, :preco, :dia, :mes, :ano, :Aerotransporte_id)
    end
end
