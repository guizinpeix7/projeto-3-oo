Rails.application.routes.draw do
 
  resources :line_items
  resources :carts
  resources :passagems
  devise_for :admin do
    get '/admin/sign_out' => 'devise/sessions#destroy'
  end
  devise_for :users do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
  
  resources :aerotransportes
  
  root 'aerotransportes#index'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
