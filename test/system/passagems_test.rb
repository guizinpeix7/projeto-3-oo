require "application_system_test_case"

class PassagemsTest < ApplicationSystemTestCase
  setup do
    @passagem = passagems(:one)
  end

  test "visiting the index" do
    visit passagems_url
    assert_selector "h1", text: "Passagems"
  end

  test "creating a Passagem" do
    visit passagems_url
    click_on "New Passagem"

    fill_in "Aerotransporte", with: @passagem.Aerotransporte_id
    fill_in "Ano", with: @passagem.ano
    fill_in "Destino", with: @passagem.destino
    fill_in "Dia", with: @passagem.dia
    fill_in "Mes", with: @passagem.mes
    fill_in "Origem", with: @passagem.origem
    fill_in "Preco", with: @passagem.preco
    click_on "Create Passagem"

    assert_text "Passagem was successfully created"
    click_on "Back"
  end

  test "updating a Passagem" do
    visit passagems_url
    click_on "Edit", match: :first

    fill_in "Aerotransporte", with: @passagem.Aerotransporte_id
    fill_in "Ano", with: @passagem.ano
    fill_in "Destino", with: @passagem.destino
    fill_in "Dia", with: @passagem.dia
    fill_in "Mes", with: @passagem.mes
    fill_in "Origem", with: @passagem.origem
    fill_in "Preco", with: @passagem.preco
    click_on "Update Passagem"

    assert_text "Passagem was successfully updated"
    click_on "Back"
  end

  test "destroying a Passagem" do
    visit passagems_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Passagem was successfully destroyed"
  end
end
