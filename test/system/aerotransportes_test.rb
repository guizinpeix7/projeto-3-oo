require "application_system_test_case"

class AerotransportesTest < ApplicationSystemTestCase
  setup do
    @aerotransporte = aerotransportes(:one)
  end

  test "visiting the index" do
    visit aerotransportes_url
    assert_selector "h1", text: "Aerotransportes"
  end

  test "creating a Aerotransporte" do
    visit aerotransportes_url
    click_on "New Aerotransporte"

    fill_in "Nome", with: @aerotransporte.nome
    click_on "Create Aerotransporte"

    assert_text "Aerotransporte was successfully created"
    click_on "Back"
  end

  test "updating a Aerotransporte" do
    visit aerotransportes_url
    click_on "Edit", match: :first

    fill_in "Nome", with: @aerotransporte.nome
    click_on "Update Aerotransporte"

    assert_text "Aerotransporte was successfully updated"
    click_on "Back"
  end

  test "destroying a Aerotransporte" do
    visit aerotransportes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Aerotransporte was successfully destroyed"
  end
end
