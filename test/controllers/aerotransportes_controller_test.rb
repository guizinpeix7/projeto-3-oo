require 'test_helper'

class AerotransportesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @aerotransporte = aerotransportes(:one)
  end

  test "should get index" do
    get aerotransportes_url
    assert_response :success
  end

  test "should get new" do
    get new_aerotransporte_url
    assert_response :success
  end

  test "should create aerotransporte" do
    assert_difference('Aerotransporte.count') do
      post aerotransportes_url, params: { aerotransporte: { nome: @aerotransporte.nome } }
    end

    assert_redirected_to aerotransporte_url(Aerotransporte.last)
  end

  test "should show aerotransporte" do
    get aerotransporte_url(@aerotransporte)
    assert_response :success
  end

  test "should get edit" do
    get edit_aerotransporte_url(@aerotransporte)
    assert_response :success
  end

  test "should update aerotransporte" do
    patch aerotransporte_url(@aerotransporte), params: { aerotransporte: { nome: @aerotransporte.nome } }
    assert_redirected_to aerotransporte_url(@aerotransporte)
  end

  test "should destroy aerotransporte" do
    assert_difference('Aerotransporte.count', -1) do
      delete aerotransporte_url(@aerotransporte)
    end

    assert_redirected_to aerotransportes_url
  end
end
