require 'test_helper'

class PassagemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @passagem = passagems(:one)
  end

  test "should get index" do
    get passagems_url
    assert_response :success
  end

  test "should get new" do
    get new_passagem_url
    assert_response :success
  end

  test "should create passagem" do
    assert_difference('Passagem.count') do
      post passagems_url, params: { passagem: { Aerotransporte_id: @passagem.Aerotransporte_id, ano: @passagem.ano, destino: @passagem.destino, dia: @passagem.dia, mes: @passagem.mes, origem: @passagem.origem, preco: @passagem.preco } }
    end

    assert_redirected_to passagem_url(Passagem.last)
  end

  test "should show passagem" do
    get passagem_url(@passagem)
    assert_response :success
  end

  test "should get edit" do
    get edit_passagem_url(@passagem)
    assert_response :success
  end

  test "should update passagem" do
    patch passagem_url(@passagem), params: { passagem: { Aerotransporte_id: @passagem.Aerotransporte_id, ano: @passagem.ano, destino: @passagem.destino, dia: @passagem.dia, mes: @passagem.mes, origem: @passagem.origem, preco: @passagem.preco } }
    assert_redirected_to passagem_url(@passagem)
  end

  test "should destroy passagem" do
    assert_difference('Passagem.count', -1) do
      delete passagem_url(@passagem)
    end

    assert_redirected_to passagems_url
  end
end
