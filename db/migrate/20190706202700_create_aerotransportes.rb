class CreateAerotransportes < ActiveRecord::Migration[5.2]
  def change
    create_table :aerotransportes do |t|
      t.string :nome

      t.timestamps
    end
  end
end
