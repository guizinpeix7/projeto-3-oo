class CreatePassagems < ActiveRecord::Migration[5.2]
  def change
    create_table :passagems do |t|
      t.string :origem
      t.string :destino
      t.float :preco
      t.float :dia
      t.string :mes
      t.float :ano
      t.references :Aerotransporte, foreign_key: true

      t.timestamps
    end
  end
end
